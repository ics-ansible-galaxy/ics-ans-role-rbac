import os
import testinfra.utils.ansible_runner
import pytest
import time


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.fixture(scope="module", autouse=True)
def wait_for_deployment(host):
    for i in range(100):
        with host.sudo():
            cmd = host.run('docker exec --env JAVA_OPTS="-Xms32m" rbac wildfly/bin/jboss-cli.sh --connect deployment-info')
            if cmd.rc == 0 and len(cmd.stdout.split('\n')) == 3 and all([deployment.split()[4] == 'OK' for deployment in cmd.stdout.split('\n')[1:-1]]):
                return
        time.sleep(3)
    raise RuntimeError('Timed out waiting for application to start.')


def test_application_container(host):
    with host.sudo():
        app = host.docker("rbac")
        assert app.is_running


def test_database_container(host):
    with host.sudo():
        database = host.docker("rbac-database")
        assert database.is_running


def test_auth_services(host):
    cmd = host.run('curl -k https://ics-ans-role-rbac-default/service/auth/roles/')
    assert '<user><roles/></user>' in cmd.stdout


def test_management_studio(host):
    cmd = host.run('curl -k https://ics-ans-role-rbac-default/')
    assert '<title>RBAC Home Page</title>' in cmd.stdout
