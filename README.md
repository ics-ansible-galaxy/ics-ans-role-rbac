# ics-ans-role-rbac

Ansible role to install RBAC.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## Role Variables

```yaml
rbac_network: rbac-network
rbac_container_name: rbac
rbac_container_image: "{{ rbac_container_image_name }}:{{ rbac_container_image_tag }}"
rbac_container_image_name: registry.esss.lu.se/ics-software/rbac
rbac_container_image_tag: latest
rbac_container_image_pull: "True"
rbac_container_volumes:
  - "/etc/pki/ca-trust/extracted/java/cacerts:/etc/pki/ca-trust/extracted/java/cacerts:ro"
rbac_env: {}
rbac_frontend_rule: "Host:{{ ansible_fqdn }}"

# To use external database host, define 'rbac_database_host'
# rbac_database_host: rbac-host
rbac_database_container_name: rbac-database
rbac_database_image: postgres:9.6.7
rbac_database_published_ports: []
rbac_database_volume: rbac-database
rbac_database_name: rbac
rbac_database_username: rbac
rbac_database_password: rbac
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-rbac
```

## License

BSD 2-clause
